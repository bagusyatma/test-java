public class Card {
    private int id;
    private String name;

    public void setId(String newId){
        this.id = Integer.parseInt(newId);
    }

    public int  getId(){
        return this.id;
    }

    public void setName(String newName){
        this.name = newName;
    }

    public String getName(){
        return this.name;
    }
}
