public class App {
    public static void main(String[] args){
        String[][] dataArr = {
                {"1", "Aku", "1", "Manager"},
                {"2", "Kamu", "2", "Karyawan"},
                {"3", "Dia", "3", "Karyawan"},
                {"4", "Lo", "4", "Karyawan"},
                {"5", "Gue", "5", "Manager"}
        };

        for (int x = 0; x < dataArr.length; x++){
            Employee data = new Employee(dataArr[x][0], dataArr[x][1], dataArr[x][2], dataArr[x][3]);
            data.showData();
            System.out.println("");
        }
    }
}

//  Parking Card : 1 - Name = Marshall
//  Name : Marshall - Salary = 3000000