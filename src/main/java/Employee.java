public class Employee extends Salary{
    Employee(String inputId, String inputName, String inputParking, String  inputPosition){
        super.setId(inputId);
        super.setName(inputName);
        super.setIdParking(inputParking);
        super.setPosition(inputPosition);
    }

    public void showData(){
        System.out.println("ID Parking : " + getIdParking() + " - Name : " + getName());
        System.out.println("Name : " + getName() + " - Salary : " + getSalary()*getId());
    }
}
