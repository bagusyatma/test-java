public class Salary extends Parking {
    private int salary;
    private String position;

    public void setPosition(String newPosition){
        this.position = newPosition;
    }

    public int getSalary(){
        if (this.position.equalsIgnoreCase("karyawan")){
            this.salary = 3000000;
        }else if (this.position.equalsIgnoreCase("manager")){
            this.salary = 10000000;
        }
        return this.salary;
    }
}
